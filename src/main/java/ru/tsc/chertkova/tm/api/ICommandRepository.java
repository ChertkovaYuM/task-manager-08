package ru.tsc.chertkova.tm.api;

import ru.tsc.chertkova.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
